<?php
/*
 * Template Name: Portfólio
 * 
 */
?>

<?php get_header(); ?>

<div class="container mt-5 px-lg-0 py-5">

    <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('template-parts/content', 'page'); ?>

    <?php endwhile; ?>

</div>

<div class="container mb-5">
    <div class="row">

        <?php
        // Query personalizada para selecionar publicações
        $portfolio = new WP_Query(array(
            'post_type' => 'portfolio',
            'posts_per_page' => 6,
        ));
        ?>

        <?php if ($portfolio->have_posts()): ?>

            <?php while ($portfolio->have_posts()): $portfolio->the_post(); ?>
                <div class="col-12 col-md-6">

                    <?php get_template_part('template-parts/content', 'portfolio'); ?>

                </div>
            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>

        <?php else: ?>
            <div class="col">
                <?php get_template_part('template-parts/content', 'none'); ?>
            </div>
        <?php endif; ?>

    </div>
    
    <div class="row">

        <div class="col d-flex justify-content-center">
            <a class="btn btn-outline-light font-weight-bold text-uppercase rounded-0" href="<?php echo get_post_type_archive_link('portfolio') ?>">
                <?php _e('See more', 'stratesign'); ?>
            </a>
        </div>

    </div>
</div>

<?php get_footer(); ?>