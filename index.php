<?php
/**
 * The main template file
 * 
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */
?>

<?php get_header(); ?>

<div class="container my-5 px-lg-0 py-5">

    <?php get_template_part('template-parts/content', 'breadcrumb'); ?>

    <div class="row">

        <div class="col">
            <?php if (have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>

                    <?php get_template_part('template-parts/content'); ?>

                <?php endwhile; ?>

            <?php else: ?>

                <?php get_template_part('template-parts/content', 'none'); ?>

            <?php endif; ?>
        </div>

    </div>

    <div class="row">

        <div class="col d-flex justify-content-center">
            <?php
            bootstrap_pagination();
            ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>