jQuery(function ($) {
    // Separator Icons Append
    $('hr.is-style-dragon').each(function (index) {
        $(this).append('<i class="icon icon-dragon">');
    });

    $('hr.is-style-fire').each(function (index) {
        $(this).append('<i class="icon icon-fire">');
    });

    $('hr.is-style-puzzle').each(function (index) {
        $(this).append('<i class="icon icon-puzzle">');
    });

    $('hr.is-style-cellphone').each(function (index) {
        $(this).append('<i class="icon icon-cellphone">');
    });

    // Bootstrap Ekko Lightbox Event Listener
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});
