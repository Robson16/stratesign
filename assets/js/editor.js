wp.domReady(() => {
    wp.blocks.registerBlockStyle('core/separator', {
        name: 'default',
        label: 'Default',
        isDefault: true
    });

    wp.blocks.registerBlockStyle('core/separator', {
        name: 'dragon',
        label: 'Dragon'
    });
    
    wp.blocks.registerBlockStyle('core/separator', {
        name: 'fire',
        label: 'Fire'
    });
    
    wp.blocks.registerBlockStyle('core/separator', {
        name: 'puzzle',
        label: 'Puzzle'
    });
    
    wp.blocks.registerBlockStyle('core/separator', {
        name: 'cellphone',
        label: 'Cell-Phone'
    });
});