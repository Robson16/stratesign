<?php
/**
 * The template for displaying all single posts
 *
 */
?>

<?php get_header(); ?>

<div class="container my-5 px-lg-0 py-5">

    <?php get_template_part('template-parts/content', 'breadcrumb'); ?>

    <?php while (have_posts()): the_post(); ?>
        <?php get_template_part('template-parts/content', 'work'); ?>
    <?php endwhile; ?>
</div>

<?php // If comments are open or we have at least one comment, load up the comment template. ?>
<?php if (comments_open() || get_comments_number()): ?>
    <section class="comments-area">
        <div class="container mt-5 px-lg-0 py-5">
            <?php comments_template(); ?>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>