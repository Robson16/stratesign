<?php

// Including the TGM Plugin Activation files
require_once get_template_directory() . '/inc/required-plugins.php';
// Including the WP Bootstrap Navwalker Class
require_once get_template_directory() . '/class/class-wp-bootstrap-navwalker.php';
// WordPress Bootstrap Pagination
require_once get_template_directory() . '/inc/wp-bootstrap-pagination.php';
// Customize Post-Type Requisition
require_once get_template_directory() . '/inc/post-types.php';
// Customize Meta-Box Requisition
require_once get_template_directory() . '/inc/meta-boxes.php';
// Filters File Requisition
require_once get_template_directory() . '/inc/filters.php';

/**
 * Queuing of style and script files
 */
function stratesign_load_scripts() {
    // CSS
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(), '4.3.1', 'all');
    wp_enqueue_style('ekko-lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css', array('bootstrap'), '5.3.0', 'all');
    wp_enqueue_style('fontello', get_template_directory_uri() . '/assets/css/fontello.css', array(), '1.0', 'all');
    wp_enqueue_style('theme', get_template_directory_uri() . '/assets/css/theme.css', array('bootstrap'), '1.0', 'all');

    // JS
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, '3.2.1');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true);
    wp_enqueue_script('ekko-lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js', array('jquery', 'bootstrap'), '5.3.0', true);
    wp_enqueue_script('theme', get_template_directory_uri() . '/assets/js/theme.js', array('jquery'), '1.0', true);

    // Social Networks Plugin Scripts
    if (is_singular() || is_home()) {
        wp_enqueue_script('twitter', get_template_directory_uri() . '/assets/js/twitter.js', array(), null, false);
        wp_enqueue_script('facebook', get_template_directory_uri() . '/assets/js/facebook.js', array(), null, false);
        wp_enqueue_script('pinterest', '//assets.pinterest.com/js/pinit.js', array(), null, false);
        wp_enqueue_script('linkedin', '//platform.linkedin.com/in.js', array(), null, false);
    }

    // Load Comment-Reply Scrypt if is Single Page
    if (is_singular()) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'stratesign_load_scripts');

/**
 * Gutenberg scripts and styles
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function stratesign_gutenberg_scripts() {
    wp_enqueue_script('st-editor', get_stylesheet_directory_uri() . '/assets/js/editor.js', array('wp-blocks', 'wp-dom'), '1.0', true);
}

add_action('enqueue_block_editor_assets', 'stratesign_gutenberg_scripts');

/**
 * Theme Setting Function
 */
function stratesign_config() {
    // Enabling translation support
    $textdomain = 'stratesign';
    load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
    load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

    // Adding theme support
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('align-wide');

    // Registering the menus
    register_nav_menus(array(
        'main_menu' => __('Main Menu', 'stratesign'),
    ));

    // Custom image sizes
    add_image_size('lg-thumbnail', 1630, 590, true);
    add_image_size('sm-thumbnail', 670, 305, true);
}

add_action('after_setup_theme', 'stratesign_config', 0);
