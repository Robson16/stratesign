<?php
/**
 * The template part for displaying single content
 */
?>

<article id="portfolio-work-<?php the_ID(); ?>" <?php post_class('post'); ?>>

    <header>
        <div class="post-infos row has-underline my-3">
            <div class="col-12 col-lg-5 d-flex align-items-center justify-content-start">
                <?php echo get_avatar(get_the_author_meta('ID'), $size = '68'); ?>
                <p class="my-0 mx-3">
                    <span class="post-author"><?php the_author_posts_link(); ?></span>
                    <br>
                    <span class="post-date"><?php echo get_the_date(); ?></span>
                </p>
            </div>
            <div class="col-12 col-lg-7">
                <p class="post-categories m-0">
                    <?php _e('Categories: ', 'stratesign'); ?>
                    <?php
                    // Get all terms\category for this post then makes a link to each one
                    $categories = wp_get_post_terms($post->ID, 'portfolio_category', array('fields' => 'all'));
                    ?>
                    <?php foreach ($categories as $category): ?>
                        <a href="<?php echo get_term_link($category->term_id); ?>" class="font-weight-bold"><?php echo $category->name; ?></a>
                    <?php endforeach; ?>
                </p>

                <p class="post-tags m-0">
                    <?php the_tags(__('Tags: ', 'stratesign'), ', '); ?>
                </p>
            </div>
        </div>

        <?php the_title(sprintf('<h1 class="post-title my-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h1>'); ?>

        <div class="row my-lg-3">
            <?php
            $infos = rwmb_meta('_stratesign_portfolio_infos');
            ?>
            <?php if ($infos[0]): ?>
                <div class="col-12 col-md-4">
                    <span><?php _e('Client: ', 'stratesign'); ?></span><span><?php echo $infos[0]; ?></span>
                </div>
            <?php endif; ?>
            <?php if ($infos[1]): ?>
                <div class="col-12 col-md-4">
                    <span class=""><?php _e('Locality: ', 'stratesign'); ?></span><span><?php echo $infos[1]; ?></span>
                </div>
            <?php endif; ?>
            <?php if ($infos[2]): ?>
                <div class="col-12 col-md-4">
                    <span><?php _e('Case: ', 'stratesign'); ?></span><span><?php echo $infos[2]; ?></span>
                </div>
            <?php endif; ?>
        </div>
    </header>

    <?php the_content(); ?>

    <div class="row">
        <?php
        $images = rwmb_meta('_stratesign_portfolio_gallery', array('size' => 'thumbnail'));
        ?>
        <?php foreach ($images as $image): ?>
            <div class="col-12 col-md-6 col-lg-4 my-3">
                <a href="<?php echo array_shift(array_values(wp_get_attachment_image_src($image[ID], 'full', false))); ?>" data-toggle="lightbox" data-gallery="portfolio-gallery">
                    <?php echo wp_get_attachment_image($image[ID], 'sm-thumbnail', false, array("class" => "img-fluid")); ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</article><!-- #portfolio-work-<?php the_ID(); ?> -->