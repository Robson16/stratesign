<?php
/**
 * Template Part para exibir o conteúdo da página em page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <h1 class="display-4 font-weight-bold text-center"><?php the_title(); ?></h1>
    <?php the_content(); ?>
</article><!-- #post-<?php the_ID(); ?> -->
