<?php
/**
 * The template part for displaying single content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header>
        <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('lg-thumbnail', array('class' => 'img-fluid', 'alt' => 'Feature Image: ' . get_the_title())); ?>
        <?php endif; ?>
        
        <div class="post-infos row has-underline my-3">
            <div class="col-12 col-md-6 col-lg-4 d-flex align-items-center justify-content-start">
                <?php echo get_avatar(get_the_author_meta('ID'), $size = '68'); ?>
                <p class="my-0 mx-3">
                    <span class="post-author"><?php the_author_posts_link(); ?></span>
                    <br>
                    <span class="post-date"><?php echo get_the_date(); ?></span>
                </p>
            </div>
            <div class="col-12 col-md-6 col-lg-8 ">
                <p class="post-categories m-0">
                    <?php _e('Categories: ', 'stratesign'); ?> 
                    <?php the_category(', '); ?>
                </p>

                <p class="post-tags m-0">
                    <?php the_tags(__('Tags: ', 'stratesign'), ', '); ?>
                </p>
            </div>
        </div>
        
        <?php the_title(sprintf('<h1 class="post-title my-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h1>'); ?>

    </header>

    <div class="post-excerpt">
        <?php the_content(); ?>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->