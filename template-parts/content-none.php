<?php
/*
 * The template part for displaying a message that posts cannot be found
 */
?>

<section class="no-results not-found">
    <header class="page-header">
        <h1 class="display-3 text-center font-weight-bold text-white"><?php _e('Nothing Found', 'stratesign'); ?></h1>
    </header><!-- .page-header -->

    <div class="page-content text-center">
        <?php if (is_home() && current_user_can('publish_posts')) : ?>

            <p><?php printf(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'stratesign'), esc_url(admin_url('post-new.php'))); ?></p>

        <?php elseif (is_search()) : ?>

            <p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'stratesign'); ?></p>

            <div class="my-5">
                <?php get_search_form(); ?>
            </div>

        <?php else : ?>

            <p><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'stratesign'); ?></p>

            <div class="my-5">
                <?php get_search_form(); ?>
            </div>

        <?php endif; ?>
    </div><!-- .page-content -->
</section><!-- .no-results -->
