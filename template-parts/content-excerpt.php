<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts.
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

    <header>
        <?php if (has_post_thumbnail()) : ?>
            <a class="post-thumbnail" aria-label="<?php _e('Read more about:', 'stratesign'); ?> <?php the_title(); ?>" href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('sm-thumbnail', array('class' => 'img-fluid', 'alt' => 'Feature Image: ' . get_the_title())); ?>
                <div class="post-overlay">
                    <p class="h4 text-uppercase text-white font-weight-light"><?php _e('Read more', 'stratesign') ?></p>
                </div>
            </a>
        <?php endif; ?>

        <?php
        if (is_front_page() && !is_home()) {
            // The excerpt is being displayed within a front page section, so it's a lower hierarchy than h2.
            the_title(sprintf('<h3 class="post-title my-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');
        } else {
            the_title(sprintf('<h2 class="post-title my-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>');
        }
        ?>

        <p class="post-infos has-underline">
            <span class="post-author"><?php the_author_posts_link(); ?></span>
            &#45;
            <span class="post-date"><?php echo get_the_date(); ?></span>
        </p>
    </header>

    <div class="post-excerpt">
        <?php the_excerpt(); ?>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->