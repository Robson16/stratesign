<?php
/**
 * Template part for displaying jobs item 
 *
 * Used in list of jobs on Portfolio Template.
 *
 */
?>

<article id="portfolio-<?php the_ID(); ?>" <?php post_class('post'); ?>>

    <?php if (has_post_thumbnail()) : ?>
        <a class="post-thumbnail" aria-label="<?php _e('Read more about:', 'stratesign'); ?> <?php the_title(); ?>" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('sm-thumbnail', array('class' => 'img-fluid', 'alt' => 'Feature Image: ' . get_the_title())); ?>
            <div class="post-overlay">
                <p class="h4 text-uppercase text-white font-weight-light"><?php _e('Read more', 'stratesign') ?></p>
            </div>
        </a>
    <?php endif; ?>

    <?php the_title(sprintf('<h3 class="post-title my-3"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>'); ?>

</article><!-- #portfolio-<?php the_ID(); ?> -->