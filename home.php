<?php
/**
 * The blog page template
 * 
 * This is the default home page for the blog, 
 * displaying the list of posts.
 *
 */
?>

<?php get_header(); ?>

<div class="container my-5 px-lg-0 py-5">
    
    <?php get_template_part('template-parts/content', 'breadcrumb'); ?>
    
    <section class="row">
        <div class="col-12 col-md-6">
            <h1 class="display-4 text-uppercase font-weight-bold has-underline"><?php _e('Blog', 'stratesign'); ?></h1>
        </div>
        <!--/.col-->
        <div class="col-12 col-md-6">
            <?php get_search_form(); ?>
        </div>
        <!--/.col-->
    </section>

    <section class="row">
        <?php
        $featured = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'offset' => 0
        ));
        ?>

        <?php if ($featured->have_posts()) : ?>

            <?php while ($featured->have_posts()) : $featured->the_post(); ?>
                <div class="col-12">
                    <?php get_template_part('template-parts/content'); ?>
                </div>
                <!--/.col-->
            <?php endwhile; ?>

            <?php // Reset the query ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>

    </section>
    <!--/.row-->

    <section class="row">

        <?php
        $secondary = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'offset' => 1
        ));
        ?>

        <?php if ($secondary->have_posts()) : ?>

            <?php while ($secondary->have_posts()) : $secondary->the_post(); ?>
                <div class="col-12 col-md-6">
                    <?php get_template_part('template-parts/content', 'excerpt'); ?>
                </div>
                <!--/.col-->
            <?php endwhile; ?>

            <?php // Reset the query ?>
            <?php wp_reset_postdata(); ?>
        <?php else: ?>
            <div class="col">
                <?php get_template_part('template-parts/content', 'none'); ?>
            </div>
        <?php endif; ?>


    </section>
    <!--/.row-->

    <section class="row">
        <div class="col-12">
            <h3 class="display-3 text-uppercase font-weight-bold has-underline"><?php _e('Tags', 'stratesign'); ?></h3>
        </div>
        <!--/.col-->

        <div class="col-12">
            <?php
            $tags = get_tags();
            $html = '<div class="tags-cloud">';
            foreach ($tags as $tag) {
                $tag_link = get_tag_link($tag->term_id);

                $html .= "<a class='tag' href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
                $html .= "{$tag->name}</a>";
            }
            $html .= '</div>';
            echo $html;
            ?>
        </div>
        <!--/.col-->
    </section>

</div>
<!--/.container-->
<?php get_footer(); ?>