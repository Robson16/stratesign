<?php

// Filters

/**
 * Add Alt and Title to the user avatar
 * 
 */
function gravatar_alt($gravatar) {
    if (have_comments()) {
        $alt = get_comment_author();
    } else {
        $alt = get_the_author_meta('display_name');
    }

    return str_replace('alt=\'\'', 'alt=\'' . $alt . '\' title=\'' . $alt . '\'', $gravatar);
}

add_filter('get_avatar', 'gravatar_alt');

/**
 * Remove website field from comment form
 * 
 */
function website_remove($fields) {
    if (isset($fields['url']))
        unset($fields['url']);
    return $fields;
}

add_filter('comment_form_default_fields', 'website_remove');

/**
 * Generete the share links and place after the_excerpt() and the_content() 
 *
 */
function stratesign_social_sharing_links($content) {
    global $post;
    if (is_singular('post') || is_home()) {

        // Get current page URL 
        $stratesignURL = urlencode(get_permalink());

        // Get current page title
        $stratesignTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');

        // Get Post Thumbnail for pinterest
        $stratesignThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

        // Construct sharing URL without using any script
        $twitterURL = 'https://twitter.com/intent/tweet?text=' . $stratesignTitle . '&amp;url=' . $stratesignURL . '&amp;via=Stratesign';
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $stratesignURL;
        $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url=' . $stratesignURL . '&amp;title=' . $stratesignTitle;
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url=' . $stratesignURL . '&amp;media=' . $stratesignThumbnail[0] . '&amp;description=' . $stratesignTitle;

        // Add sharing button at the end of page/page content
        $content .= '<div class="share-social">';
        $content .= '<a class="share-link facebook" href="' . $facebookURL . '" target="_blank"><i class="icon-facebook"></i></a>';
        $content .= '<a class="share-link linkedin" href="' . $linkedInURL . '" target="_blank"><i class="icon-linkedin"></i></a>';
        $content .= '<a class="share-link twitter" href="' . $twitterURL . '" target="_blank"><i class="icon-twitter"></i></a>';
        $content .= '<a class="share-link pinterest" href="' . $pinterestURL . '" data-pin-custom="true" target="_blank"><i class="icon-pinterest"></i></a>';
        $content .= '&nbsp;&nbsp;&nbsp;&#124;&nbsp;&nbsp;&nbsp;';
        $content .= '<span class="font-weight-bold comment-count"><i class="icon-comment"></i>' . get_comments_number() . '</span>';
        $content .= '</div>';

        return $content;
    } else {
        // if not a post/page then don't include sharing button
        return $content;
    }
}

add_filter('the_excerpt', 'stratesign_social_sharing_links', 10);
add_filter('the_content', 'stratesign_social_sharing_links', 15);

/**
 * Remove archive labels.
 * 
 * @param  string $title Current archive title to be displayed.
 * @return string        Modified archive title to be displayed.
 */
function my_theme_archive_title($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    }

    return $title;
}

add_filter('get_the_archive_title', 'my_theme_archive_title');
