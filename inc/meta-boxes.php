<?php

// Theme Pages MetaBox
function stratesign_pages_meta_box($meta_boxes) {
    $prefix = '_stratesign_page_';

    $meta_boxes[] = array(
        'id' => 'background',
        'title' => esc_html__('Background', 'stratesign'),
        'post_types' => array('page'),
        'context' => 'side',
        'priority' => 'default',
        'autosave' => 'true',
        'fields' => array(
            array(
                'id' => $prefix . 'bg_img',
                'type' => 'image_advanced',
                'desc' => esc_html__('Set the background image', 'stratesign'),
                'max_file_uploads' => '1',
                'max_status' => 'true',
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'stratesign_pages_meta_box');

// Theme Portfolio MetaBox
function stratesign_portfolio_meta_box($meta_boxes) {
    $prefix = '_stratesign_portfolio_';

    $meta_boxes[] = array(
        'id' => 'portfolio_job',
        'title' => esc_html__('Job Details', 'stratesign'),
        'post_types' => array('portfolio'),
        'context' => 'after_title',
        'priority' => 'high',
        'autosave' => 'true',
        'fields' => array(
            array(
                'id' => $prefix . 'gallery',
                'type' => 'image_advanced',
                'name' => esc_html__('Gallery', 'stratesign'),
                'desc' => esc_html__('Gallery of images', 'stratesign'),
            ),
            array(
                'id' => $prefix . 'infos',
                'type' => 'text_list',
                'name' => esc_html__('Job Information', 'stratesign'),
                'options' => array(
                    'client' => esc_html__('Client', 'stratesign'),
                    'locality' => esc_html__('Locality', 'stratesign'),
                    'case' => esc_html__('Case', 'stratesign'),
                ),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'stratesign_portfolio_meta_box');
