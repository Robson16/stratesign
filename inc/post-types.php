<?php

// Register Custom Post Type
function stratesign_portfolio() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'stratesign' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'stratesign' ),
		'menu_name'             => __( 'Portfolio', 'stratesign' ),
		'name_admin_bar'        => __( 'Portfolio', 'stratesign' ),
		'archives'              => __( 'Item Archives', 'stratesign' ),
		'attributes'            => __( 'Item Attributes', 'stratesign' ),
		'parent_item_colon'     => __( 'Parent Item:', 'stratesign' ),
		'all_items'             => __( 'All Items', 'stratesign' ),
		'add_new_item'          => __( 'Add New Item', 'stratesign' ),
		'add_new'               => __( 'Add New', 'stratesign' ),
		'new_item'              => __( 'New Item', 'stratesign' ),
		'edit_item'             => __( 'Edit Item', 'stratesign' ),
		'update_item'           => __( 'Update Item', 'stratesign' ),
		'view_item'             => __( 'View Item', 'stratesign' ),
		'view_items'            => __( 'View Items', 'stratesign' ),
		'search_items'          => __( 'Search Item', 'stratesign' ),
		'not_found'             => __( 'Not found', 'stratesign' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'stratesign' ),
		'featured_image'        => __( 'Featured Image', 'stratesign' ),
		'set_featured_image'    => __( 'Set featured image', 'stratesign' ),
		'remove_featured_image' => __( 'Remove featured image', 'stratesign' ),
		'use_featured_image'    => __( 'Use as featured image', 'stratesign' ),
		'insert_into_item'      => __( 'Insert into item', 'stratesign' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'stratesign' ),
		'items_list'            => __( 'Items list', 'stratesign' ),
		'items_list_navigation' => __( 'Items list navigation', 'stratesign' ),
		'filter_items_list'     => __( 'Filter items list', 'stratesign' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'stratesign' ),
		'description'           => __( 'Works in Portfolio', 'stratesign' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
		'taxonomies'            => array( 'portfolio_category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'stratesign_portfolio', 0 );

// Register Custom Taxonomy
function stratesign_portfolio_category() {

	$labels = array(
		'name'                       => _x( 'Portfolio Categories', 'Taxonomy General Name', 'stratesign' ),
		'singular_name'              => _x( 'Portfolio Category', 'Taxonomy Singular Name', 'stratesign' ),
		'menu_name'                  => __( 'Portfolio Category', 'stratesign' ),
		'all_items'                  => __( 'All Items', 'stratesign' ),
		'parent_item'                => __( 'Parent Item', 'stratesign' ),
		'parent_item_colon'          => __( 'Parent Item:', 'stratesign' ),
		'new_item_name'              => __( 'New Item Name', 'stratesign' ),
		'add_new_item'               => __( 'Add New Item', 'stratesign' ),
		'edit_item'                  => __( 'Edit Item', 'stratesign' ),
		'update_item'                => __( 'Update Item', 'stratesign' ),
		'view_item'                  => __( 'View Item', 'stratesign' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'stratesign' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'stratesign' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'stratesign' ),
		'popular_items'              => __( 'Popular Items', 'stratesign' ),
		'search_items'               => __( 'Search Items', 'stratesign' ),
		'not_found'                  => __( 'Not Found', 'stratesign' ),
		'no_terms'                   => __( 'No items', 'stratesign' ),
		'items_list'                 => __( 'Items list', 'stratesign' ),
		'items_list_navigation'      => __( 'Items list navigation', 'stratesign' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

}
add_action( 'init', 'stratesign_portfolio_category', 0 );