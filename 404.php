<?php
/**
 * The template for displaying 404 pages (not found)
 */
?>

<?php get_header(); ?>

<div class="container my-lg-5 px-lg-0 py-5">
    <header class="page-header">
        <h2 class="text404 font-weight-bold text-center m-0">404</h2>
        <h1 class="page-title font-weight-light text-center">
            <?php _e('Page not found.', 'stratesign'); ?>
        </h1>
    </header>

    <div class="page-content">
        <p class="font-weight-bold text-center">
            <?php _e('Do not be sad, use the search bar to find what you like.', 'stratesign'); ?>
        </p>
    </div>

    <div class="my-5">
        <?php get_search_form(); ?>
    </div>
</div>

<?php get_footer(); ?>