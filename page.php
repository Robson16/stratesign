<?php
/**
 * The template for generic pages
 */
?>

<?php get_header(); ?>

<div class="container my-5 px-lg-0 py-5">

    <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('template-parts/content', 'page'); ?>

    <?php endwhile; ?>

</div>

<?php get_footer(); ?>