<?php
/**
 * The template for displaying archive pages
 *
 */
?>

<?php get_header(); ?>

<div class="container my-5 px-lg-0 py-5">

    <?php get_template_part('template-parts/content', 'breadcrumb'); ?>

    <div class="row">

        <div class="col-12">
            <h1 class="display-4 text-uppercase font-weight-bold has-underline">
                <?php echo post_type_archive_title( '', false ); ?>
            </h1>
        </div>
        <!--/.col-->


        <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>

                <div class="col-12 col-md-6">
                    <?php get_template_part('template-parts/content', 'portfolio'); ?>
                </div>

            <?php endwhile; ?>



        <?php else: ?>

            <?php get_template_part('template-parts/content', 'none'); ?>

        <?php endif; ?>


    </div>

    <div class="row">

        <div class="col d-flex justify-content-center">
            <?php
            bootstrap_pagination();
            ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>